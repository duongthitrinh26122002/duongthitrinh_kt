﻿using System;
using System.Collections.Generic;

namespace CodeFirst.Models;

public partial class Customer
{
    public int CustomerId { get; set; }

    public string? Firstname { get; set; }

    public string? Lastname { get; set; }

    public string? ContactAndAddress { get; set; }

    public virtual ICollection<Account> Accounts { get; } = new List<Account>();

    public virtual ICollection<Transaction> Transactions { get; } = new List<Transaction>();
}
