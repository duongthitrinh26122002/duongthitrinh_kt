﻿using System;
using System.Collections.Generic;

namespace CodeFirst.Models;

public partial class Employee
{
    public int EmployeeId { get; set; }

    public string? Firstname { get; set; }

    public string? Lastname { get; set; }

    public string? ContactAndAddress { get; set; }

    public virtual ICollection<Transaction> Transactions { get; } = new List<Transaction>();
}
