﻿using System;
using System.Collections.Generic;

namespace CodeFirst.Models;

public partial class Account
{
    public int AccountId { get; set; }

    public int? CustomerId { get; set; }

    public string? Accountname { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual ICollection<Report> Reports { get; } = new List<Report>();
}
