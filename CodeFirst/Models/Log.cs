﻿using System;
using System.Collections.Generic;

namespace CodeFirst.Models;

public partial class Log
{
    public int LogsId { get; set; }

    public int? TransationId { get; set; }

    public DateTime? Logindate { get; set; }

    public TimeSpan? Logintime { get; set; }

    public virtual ICollection<Report> Reports { get; } = new List<Report>();

    public virtual Transaction? Transation { get; set; }
}
